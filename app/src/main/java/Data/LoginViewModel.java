package Data;

import android.util.Patterns;

import androidx.lifecycle.ViewModel;

import com.example.actividad.R;

public class LoginViewModel extends ViewModel  {
    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && !password.trim().isEmpty();
    }

    public int loginDataChanged(String username, String password) {

        int msj = 0;
        if (!isUserNameValid(username)) {
            msj = R.string.invalid_username;
        } else if (!isPasswordValid(password)) {
            msj = R.string.invalid_password;
        }

        return msj;
    }
}
