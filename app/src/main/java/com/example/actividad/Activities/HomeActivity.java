package com.example.actividad.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.actividad.R;

import java.util.Objects;


public class HomeActivity extends AppCompatActivity {

    TextView txtNombre;
    Button btnLogOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getControls();

        String nombre = Objects.requireNonNull(getIntent().getExtras()).getString("nombre", null);

        txtNombre.setText(nombre);

        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void getControls() {
        txtNombre = findViewById(R.id.txt_home);
        btnLogOut = findViewById(R.id.btnLogOut);
    }
}
