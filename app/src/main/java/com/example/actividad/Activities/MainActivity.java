package com.example.actividad.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.actividad.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Data.LoginViewModel;
import Data.VolleyS;

public class MainActivity extends AppCompatActivity {

    EditText usernameEditText;
    EditText passwordEditText;
    Button loginButton;
    ProgressBar loadingProgressBar;
    private LoginViewModel loginViewModel;
    int msj;

    private VolleyS volley;
    protected RequestQueue fRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginViewModel = new LoginViewModel();
        volley = VolleyS.getInstance(this.getApplicationContext());
        fRequestQueue = volley.getRequestQueue();

        getControls();
        listener();

    }

    private void getControls() {

        usernameEditText = findViewById(R.id.txtnames);
        passwordEditText = findViewById(R.id.txtpwd);
        loginButton = findViewById(R.id.login);
        loadingProgressBar = findViewById(R.id.loading);

        initialError();

    }

    private void initialError() {
        usernameEditText.setError(null);
        passwordEditText.setError(null);
    }

    private void listener(){

        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE ) {

                    initialError();
                    loadingProgressBar.setVisibility(View.VISIBLE);
                    msj = loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());

                    showMsj(msj);

                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                initialError();
                loadingProgressBar.setVisibility(View.VISIBLE);
                msj = loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());

                showMsj(msj);

            }
        });
    }

    private void showMsj(int msj) {
        switch(msj) {
            case R.string.invalid_username:
                loadingProgressBar.setVisibility(View.INVISIBLE);
                usernameEditText.setError(getResources().getString(msj));
                break;
            case R.string.invalid_password:
                loadingProgressBar.setVisibility(View.INVISIBLE);
                passwordEditText.setError(getResources().getString(msj));
                break;
            default:
                makeRequest();
                break;
        }

    }

    private void login(String nombre) {

        clearControls();
        Intent intent = new Intent(this, HomeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("nombre",nombre);
        intent.putExtras(bundle);
        startActivity(intent);
        Toast.makeText(getBaseContext(), R.string.welcome, Toast.LENGTH_LONG).show();


    }

    private void clearControls() {
        usernameEditText.setText(null);
        passwordEditText.setText(null);
    }

    public void addToQueue(Request request) {
        if (request != null) {
            request.setTag(this);
            if (fRequestQueue == null)
                fRequestQueue = volley.getRequestQueue();
                request.setRetryPolicy(new DefaultRetryPolicy(
                        60000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
            onPreStartConnection();
            fRequestQueue.add(request);
        }
    }

    public void onPreStartConnection() {
        this.setProgressBarIndeterminateVisibility(true);
    }

    public void onConnectionFinished() {
        this.setProgressBarIndeterminateVisibility(false);
    }

    public void onConnectionFailed(String error) {
        this.setProgressBarIndeterminateVisibility(false);
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    private void makeRequest(){
        String url = getString(R.string.URL);

        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        loadingProgressBar.setVisibility(View.INVISIBLE);

                        try {
                            JSONObject obj = new JSONObject(response);


                            Toast.makeText(getApplicationContext(), "Response:" + response, Toast.LENGTH_SHORT).show();

                            if(!obj.isNull("name")){
                                login(obj.getString("name"));
                            }else{
                                Toast.makeText(getBaseContext(), R.string.login_failed, Toast.LENGTH_LONG).show();
                            }

                            onConnectionFinished();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loadingProgressBar.setVisibility(View.INVISIBLE);
                        onConnectionFailed(error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                params.put("username",usernameEditText.getText().toString());
                params.put("password", passwordEditText.getText().toString());

                return params;
            }
        };

        addToQueue(request);

    }



}
